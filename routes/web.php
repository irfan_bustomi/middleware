<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::middleware(['auth', 'admin'])->group(function(){
    Route::get('/admin', 'VerificationController@admin');
});

Route::middleware(['auth', 'email_verified'])->group(function(){
    Route::get('/route-1', 'VerificationController@email_verified');
});

Route::middleware(['auth', 'email_verified'])->group(function(){
    Route::get('/route-2', 'VerificationController@email_verified');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
