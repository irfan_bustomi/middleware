<?php

use Illuminate\Database\Seeder;
use App\Roles;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Roles::create([
            'id' => '0',
            'name' => 'admin',
            'name' => 'admin',
            'slug' => '',
            'created_at' => '2021-02-18 01:28:56',
            'updated_at' => '2021-02-18 01:28:56'
        ]);
        Roles::create([
            'id' => '1',
            'name' => 'user',
            'slug' => '',
            'created_at' => '2021-02-18 01:28:56',
            'updated_at' => '2021-02-18 01:28:56'
        ]);
    }
}
